package com.hl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SimplePOST extends Activity {
 private TextView show;
 private EditText txt;
 private Button btn;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        show = (TextView)findViewById(R.id.show);
        txt = (EditText)findViewById(R.id.txt);
        btn = (Button)findViewById(R.id.btn);
        btn.setOnClickListener(new OnClickListener() {
   
   @Override
   public void onClick(View v) {
    dopost(txt.getText().toString());
    
   }
  });
    }

 private void dopost(String val){
     //封装数据
     Map<String, String> parmas = new HashMap<String, String>();
     parmas.put("name", val);
    
     DefaultHttpClient client = new DefaultHttpClient();//http客户端
     HttpPost httpPost = new HttpPost("http://mhycoe.com/test/post.php");
    
     ArrayList<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
     if(parmas != null){
         Set<String> keys = parmas.keySet();
         for(Iterator<String> i = keys.iterator(); i.hasNext();) {
              String key = (String)i.next();
              pairs.add(new BasicNameValuePair(key, parmas.get(key)));
         }
    }
    
  try {
   UrlEncodedFormEntity p_entity = new UrlEncodedFormEntity(pairs, "utf-8");
         /*
          *  将POST数据放入HTTP请求
          */
         httpPost.setEntity(p_entity);
         /*
          *  发出实际的HTTP POST请求
           */
         HttpResponse response = client.execute(httpPost);
         HttpEntity entity = response.getEntity();
         InputStream content = entity.getContent();
   String returnConnection = convertStreamToString(content);
         show.setText(returnConnection);
  } catch (IllegalStateException e) {
   e.printStackTrace();
  } catch (IOException e) {
   e.printStackTrace();
  }
    
 }

  private String convertStreamToString(InputStream is) {
   BufferedReader reader = new BufferedReader(new InputStreamReader(is));
         StringBuilder sb = new StringBuilder();
         String line = null;
         try {
              while ((line = reader.readLine()) != null) {
                   sb.append(line);
              }
         } catch (IOException e) {
              e.printStackTrace();
         } finally {
              try {
                   is.close();
              } catch (IOException e) {
                   e.printStackTrace();
              }
         }
         return sb.toString();
 }
}